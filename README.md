# PostmarketOS Pinephone Modem Fixer

This is a hack to try to quickly get the modem on the pinephone working again
after a bad suspend/resume where the modem crashed.  It is intended to be
run from postmarketOS and basically just restarts the modemmanager and eg25-manager
services until a modem becomes available.

I don't know if this will work in all situations.  We'll see.

![](screenshot.png?raw=true)
